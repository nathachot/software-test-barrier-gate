import server from './server'

const BarrierGate_left = async () => {
  const res = await server('').post('/barrier/left')
  return res
}

const BarrierGate_right = async () => {
  const res = await server('').post('/barrier/right')
  return res
}

export {
  BarrierGate_left,
  BarrierGate_right
}