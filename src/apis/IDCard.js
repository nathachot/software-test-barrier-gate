import server from './server'

const ReadCard = async () => {
  const res = await server('').post('/idcard/data')
  return res
}

export {
  ReadCard
}