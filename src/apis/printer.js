import server from './server'

const ErrorCauseStatus = async () => {
  const res = await server('').post('/ErrorCauseStatus')
  return res
}

const RollPaperSensorStatus = async () => {
  const res = await server('').post('/RollPaperSensorStatus')
  return res
}

const PrinterStatus = async () => {
  const res = await server('').post('/PrinterStatus')
  return res
}


const OfflineCauseStatus = async () => {
  const res = await server('').post('/OfflineCauseStatus')
  return res
}

const Print = async () => {
  const res = await server('').post('/Print', {
    "text": "Software Test\n\n!@#$%^^&*&~+_)(*(\n\n\n!@#$%^^&*&~+_)(*(\n\n\n\nTest"
  })
  return res
}

const FullCut = async () => {
  const res = await server('').post('/FullCut')
  return res
}


export {
  ErrorCauseStatus,
  RollPaperSensorStatus,
  PrinterStatus,
  OfflineCauseStatus,
  Print,
  FullCut
}