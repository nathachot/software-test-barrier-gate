import axios from 'axios'

// /eslint-disable-next-line
export default function (token) {
  var api = axios.create({
    baseURL: 'http://127.0.0.1:1111/api'
    // baseURL: 'http://192.168.2.222:1111/api'
  })
  api.defaults.headers.common['Authorization'] = 'Bearer ' + token
  return api
}