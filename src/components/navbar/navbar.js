import React, { useState } from "react";
import Button from '@material-ui/core/Button';
import { Link } from "react-router-dom"; 


function Navbar() {
    const [click, setClick] = useState(false);
    const closeMobileMenu = () => setClick(false);
    return (
        <div  className="navbar-printer">
            <Link to='/' onClick={closeMobileMenu}>
                <Button style={{color:"#fff"}} href="#" className="col-sm-4">
                    <div>Printer</div>
                </Button>
            </Link>
            <Link to='/ID-Card' onClick={closeMobileMenu}>
                <Button style={{color:"#fff"}} href="#" className="col-sm-4">
                    <div>ID-Card</div>
                </Button>
            </Link>
            <Link to='/Barrier-Gate' onClick={closeMobileMenu}>
                <Button style={{color:"#fff"}} href="#" className="col-sm-4">
                    <div>Barrier-Gate</div>
                </Button>
            </Link>
        </div>
    )
}

export default Navbar;