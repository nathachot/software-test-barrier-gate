import React from "react";
import Button from '@material-ui/core/Button';
import {   
    BarrierGate_left,
    BarrierGate_right
} from './../../apis/Barrier-gate'

import './Barrier-Gate.style.css'

class BarrierGateLeft extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            left_msg: "-",
            right_msg: "-"
        }
        this.handleOpenLeft = this.handleOpenLeft.bind(this)
        this.handleOpenRight = this.handleOpenRight.bind(this)
    }

    async handleOpenLeft() {
        const res = await BarrierGate_left()
        console.log(res.data.msg)
        this.setState({
            left_msg: res.data.msg
        })
    }
    async handleOpenRight() {
        const res = await BarrierGate_right()
        console.log(res.data.msg)
        this.setState({
            right_msg: res.data.msg
        })
    }

    componentWillMount() {

    }

    render() {
        return (
            <React.Fragment>
                <div className="container-fluid">
                    <fieldset className="scheduler-border">
                        <legend className="scheduler-border">
                            <div className="font-size-text">Door Left</div>
                        </legend>
                        <div className="control-group">
                            <div className="controls bootstrap-timepicker">
                                <div className="box-body">
                                <div className="container-fluid">
                                        <div className="row">
                                            <div className="col-sm-6">
                                                <Button style={{float: "right",fontSize: 25}}  onClick={this.handleOpenLeft} variant="contained" color="primary" component="span">Open Door</Button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="container-fluid">
                                        <div className="row">
                                        
                                            <div className="col-sm-6">
                                                <div className="font-size-text" style={{float: "right"}}>Message :</div>
                                            </div>
                                            <div className="col-sm-6">
                                                <div className="font-size-text" >
                                                    {
                                                        this.state.left_msg
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>

                <div className="container-fluid">
                     <fieldset className="scheduler-border">
                         <legend className="scheduler-border">
                             <div className="font-size-text">Door right</div>
                         </legend>
                         <div className="control-group">
                             <div className="controls bootstrap-timepicker">
                                 <div className="box-body">
                                     <div className="row">
                                         <div className="col-sm-6">
                                             <Button style={{float: "right", fontSize: 25}}  onClick={this.handleOpenRight} variant="contained" color="primary" component="span">Open Door</Button>
                                         </div>
                                     </div>
                                     <div className="container-fluid">
                                         <div className="row">
                                             <div className="col-sm-6">
                                                 <div className="font-size-text" style={{float: "right"}}>Message :</div>
                                             </div>
                                             <div className="col-sm-6">
                                                <div className="font-size-text" >
                                                    {
                                                        this.state.right_msg
                                                    }
                                                </div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </fieldset>
                 </div>
            </React.Fragment>
        )
    }
}


export default BarrierGateLeft 