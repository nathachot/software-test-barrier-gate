import React from 'react';
import '../../App.css';



class QRCode extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
    }
    componentDidMount() {
        this.textInput.current.focus();
    }
    
    render() {
        return (
            <React.Fragment>
                <div className="container-fluid">
                    <fieldset className="scheduler-border">
                        <legend className="scheduler-border">
                            <div className="font-size-text">QR-code</div>
                        </legend>
                        <div className="control-group">
                            <div className="controls bootstrap-timepicker">
                                <div className="box-body">
                                    <div className="container-fluid">
                                        <div className="row">
                                            <div className="col-sm-6">
                                                <div className="font-size-text" style={{float: "right"}}>Data :</div>
                                            </div>
                                            <div className="col-sm-6">
                                            <input className="form-control size" ref={this.textInput}  />
                                                {/* <div style={{color: "#70e000"}}></div> */}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </React.Fragment>
        )
    }
}

export default QRCode;