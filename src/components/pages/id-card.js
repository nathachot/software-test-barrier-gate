import React from "react";
import {   
    ReadCard
} from './../../apis/IDCard'
import './id-card.style.css'

var intervalTime = Object 

class IDCard extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            IDCard: [],
            IDCardOffline: true
        }
    }

    async handleLoadData () {
        console.log('Data ID Card')
        var res = await ReadCard()
        this.setState({
            IDCard: [res.data]
        });
        // console.log(res.data.msg[0])
        if (res.data.msg[0].connecttion){
            this.setState({
                IDCardOffline: true
            });
        }else {
            this.setState({
                IDCardOffline: false
            });
        }
    }
    handleIntervalData () {
        var self = this
        intervalTime = setInterval(function(){
            self.handleLoadData()
        }, 2000 );
    }

    componentWillMount() {
        this.handleLoadData()
        this.handleIntervalData()
    }

    componentWillUnmount (){
        clearInterval(intervalTime)
    }

    showContent () {
        return <>
            <div className="row">
                <div className="col-sm-12 set-center">
                    <div>
                        <table>
                            <tbody>
                                <tr>
                                    <td>Fullname :</td>
                                    <td>{this.state.IDCard[0].msg[0].fullname}</td>
                                </tr>
                                <tr>
                                    <td>ID Card: </td>
                                    <td>{this.state.IDCard[0].msg[0].idcard}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </>
    }
    render() {
        return (
            <React.Fragment>
                <div hidden={this.state.IDCardOffline} className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="status-idcard-offline">
                                <div>ID Card Reader Offline [ Please plug in the ID Card Reader ]</div>
                                <br></br>
                                {
                                    ((this.state.IDCard[0] !== undefined) ? <div className="error-msg">{this.state.IDCard[0].msg[0].msg}</div>: "")
                                }
                            </div>
                        </div>
                    </div>
                </div>

                <div hidden={!this.state.IDCardOffline} className="container-fluid">
                    <fieldset className="scheduler-border">
                        <legend className="scheduler-border">
                            <div className="font-size-text">ID-Card</div>
                        </legend>
                        <div className="control-group">
                            <div className="controls bootstrap-timepicker">
                                {
                                    ((this.state.IDCard[0] !== undefined && this.state.IDCard[0].msg[0].connecttion) ? this.showContent(): "disconnect from ID card reader.")
                                }
                            </div>
                        </div>
                    </fieldset>
                </div>
            </React.Fragment>
        )
    }
}
export default IDCard;