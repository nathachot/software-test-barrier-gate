import '../../App.css';
import React from "react";
import Button from '@material-ui/core/Button';
import {   
    ErrorCauseStatus,
    RollPaperSensorStatus,
    PrinterStatus,
    OfflineCauseStatus,
    Print,
    FullCut 
} from './../../apis/printer'

import './printer.style.css'

var intervalTime = Object

class Printer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            PrinterStatus: [],
            RollPaperStatus: [],
            ErrorCauseStatus: [],
            OfflineCauseStatus: [],
            PrinterOffline: true,
        }
    }

    async handlePrint(){
        await Print()
    }
    async handleCut(){
        await FullCut()
    }

    async handleLoadData () {
        console.log('Data Printer')
        var res = await ErrorCauseStatus()
        this.setState({
            ErrorCauseStatus: [res.data]
        });
        if (res.data.connection){
            this.setState({
                PrinterOffline: false
            });
        }else {
            // true
            this.setState({
                PrinterOffline: true
            });
        }
        res = await OfflineCauseStatus()
        this.setState({
            OfflineCauseStatus: [res.data]
        });
        res = await RollPaperSensorStatus()
        this.setState({
            RollPaperStatus: [res.data]
        });
        res = await PrinterStatus()
        this.setState({
            PrinterStatus: [res.data]
        });
    }

    handleIntervalData () {
        var self = this
        intervalTime = setInterval(function(){
            self.handleLoadData()
        }, 2000 );
    }
    componentWillMount() {
        this.handleLoadData()
        this.handleIntervalData()
    }
    componentWillUnmount (){
        clearInterval(intervalTime)
    }

    selectBitStatus (bit, name) {
        return this.state[name][0].res.statuses.filter(item => item.bit === bit)
    }

    PrinterStatus () {
        var s1 = this.selectBitStatus(2, 'PrinterStatus')
        var s2 = this.selectBitStatus(6, 'PrinterStatus')
        return <>
            <div className="row">
                <div className="col-sm-12">
                    <table>
                        <tbody>
                        <tr>
                            <td>Not waiting for online recovery :</td>
                            <td>
                            {
                                ((s1[0].status === 'ok') ? <div className="status-ok">OK</div>
                                : ((s1[0].status === 'error') ? <div className="status-error">Error</div>
                                    : ((s1[0].status === 'warning') ? <div className="status-warning">Warning</div>
                                        : "null")))
                            }
                            </td>
                        </tr>
                        <tr>
                            <td>Paper feed button is not being pressed :</td>
                            <td>
                            {
                                ((s2[0].status === 'ok') ? <div className="status-ok">OK</div>
                                : ((s2[0].status === 'error') ? <div className="status-error">Error</div>
                                    : ((s2[0].status === 'warning') ? <div className="status-warning">Warning</div>
                                        : "null")))
                            }
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    }

    ErrorCauseStatus () {
        var s1 = this.selectBitStatus(3, 'ErrorCauseStatus')
        var s2 = this.selectBitStatus(2, 'ErrorCauseStatus')
        var s3 = this.selectBitStatus(5, 'ErrorCauseStatus')
        var s4 = this.selectBitStatus(6, 'ErrorCauseStatus')
        return <>
            <div className="row">
                <div className="col-sm-12">
                    <table>
                        <tbody>
                        <tr>
                            <td>No autocutter error :</td>
                            <td>
                            {
                                ((s1 === 'ok') ? <div className="status-ok">OK</div>
                                : ((s1 === 'error') ? <div className="status-error">Error</div>
                                    : ((s1 === 'warning') ? <div className="status-warning">Warning</div>
                                        : "null")))
                            }
                            </td>
                        </tr>
                        <tr>
                            <td>no recoverable error :</td>
                            <td>
                            {
                                ((s2 === 'ok') ? <div className="status-ok">OK</div>
                                : ((s2 === 'error') ? <div className="status-error">Error</div>
                                    : ((s2 === 'warning') ? <div className="status-warning">Warning</div>
                                        : "null")))
                            }
                            </td>
                        </tr>

                        <tr>
                            <td>No unrecoverable error :</td>
                            <td>
                            {
                                ((s3 === 'ok') ? <div className="status-ok">OK</div>
                                : ((s3 === 'error') ? <div className="status-error">Error</div>
                                    : ((s3 === 'warning') ? <div className="status-warning">Warning</div>
                                        : "null")))
                            }
                            </td>
                        </tr>

                        <tr>
                            <td>No auto recoverable error :</td>
                            <td>
                            {
                                ((s4 === 'ok') ? <div className="status-ok">OK</div>
                                : ((s4 === 'error') ? <div className="status-error">Error</div>
                                    : ((s4 === 'warning') ? <div className="status-warning">Warning</div>
                                        : "null")))
                            }
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    }

    RollPaperStatus () {
        var s1 = this.selectBitStatus(2, 'RollPaperStatus')
        var s2 = this.selectBitStatus(6, 'RollPaperStatus')
        return <>
            <div className="row">
                <div className="col-sm-12">
                    <table>
                        <tbody>
                        <tr>
                            <td>Roll paper near-end sensor :</td>
                            <td>
                            {
                                ((s1[0].status === 'ok') ? <div className="status-ok">OK</div>
                                : ((s1[0].status === 'error') ? <div className="status-error">Error</div>
                                    : ((s1[0].status === 'warning') ? <div className="status-warning">Warning</div>
                                        : "null")))
                            }
                            </td>
                        </tr>
                        <tr>
                            <td>Roll paper end :</td>
                            <td>
                            {
                                ((s2[0].status === 'ok') ? <div className="status-ok">OK</div>
                                : ((s2[0].status === 'error') ? <div className="status-error">Error</div>
                                    : ((s2[0].status === 'warning') ? <div className="status-warning">Warning</div>
                                        : "null")))
                            }
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    }

    OfflineCauseStatus () {
        var s1 = this.selectBitStatus(2, 'OfflineCauseStatus')
        var s2 = this.selectBitStatus(3, 'OfflineCauseStatus')
        var s3 = this.selectBitStatus(5, 'OfflineCauseStatus')
        var s4 = this.selectBitStatus(6, 'OfflineCauseStatus')
        return <>
            <div className="row">
                <div className="col-sm-12">
                    <table>
                        <tbody>
                        <tr>
                            <td>Cover is Closed :</td>
                            <td>
                            {
                                ((s1[0].status === 'ok') ? <div className="status-ok">OK</div>
                                : ((s1[0].status === 'error') ? <div className="status-error">Error</div>
                                    : ((s1[0].status === 'warning') ? <div className="status-warning">Warning</div>
                                        : "null")))
                            }
                            </td>
                        </tr>
                        <tr>
                            <td>Paper is not being fed by the paper feed button :</td>
                            <td>
                            {
                                ((s2[0].status === 'ok') ? <div className="status-ok">OK</div>
                                : ((s2[0].status === 'error') ? <div className="status-error">Error</div>
                                    : ((s2[0].status === 'warning') ? <div className="status-warning">Warning</div>
                                        : "null")))
                            }
                            </td>
                        </tr>

                        <tr>
                            <td>Printing stops due to a paper-end :</td>
                            <td>
                            {
                                ((s3[0].status === 'ok') ? <div className="status-ok">OK</div>
                                : ((s3[0].status === 'error') ? <div className="status-error">Error</div>
                                    : ((s3[0].status === 'warning') ? <div className="status-warning">Warning</div>
                                        : "null")))
                            }
                            </td>
                        </tr>

                        <tr>
                            <td>No error :</td>
                            <td>
                            {
                                ((s4[0].status === 'ok') ? <div className="status-ok">OK</div>
                                : ((s4[0].status === 'error') ? <div className="status-error">Error</div>
                                    : ((s4[0].status === 'warning') ? <div className="status-warning">Warning</div>
                                        : "null")))
                            }
                            </td>
                        </tr>


                        </tbody>
                    </table>
                </div>
            </div>
        </>
    }



    render() {
        return (
            <React.Fragment>
                {/* show printer offline  */}
                <div hidden={!this.state.PrinterOffline} className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="status-printer-offline">
                                Printer Offline [ Please plug in the printer ]
                            </div>
                        </div>
                    </div>
                </div>
                <div hidden={this.state.PrinterOffline} className="container-fluid">
                    <div className="row">
                        <div className="col-sm-6">
                            <fieldset className="scheduler-border">
                                <legend className="scheduler-border"><div>PrinterStatus</div></legend>
                                <div className="control-group">
                                    <div className="controls bootstrap-timepicker">
                                        {
                                            ((this.state.PrinterStatus[0] !== undefined 
                                                && this.state.PrinterStatus[0].connection) ? 
                                                    this.PrinterStatus() 
                                                        : "disconnect from printer.")
                                        }
                                    </div>
                                </div>
                            </fieldset>
                        </div>

                        <div className="col-sm-6">
                            <fieldset className="scheduler-border">
                                <legend className="scheduler-border"><div>RollPaperStatus</div></legend>
                                <div className="control-group">
                                    <div className="controls bootstrap-timepicker">
                                        {
                                            ((this.state.RollPaperStatus[0] !== undefined 
                                                && this.state.RollPaperStatus[0].connection) ? 
                                                    this.RollPaperStatus()
                                                        : "disconnect from printer.")
                                        }
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>

                <div hidden={this.state.PrinterOffline} className="container-fluid">
                    <div className="row">
                        <div className="col-sm-6">
                            <fieldset className="scheduler-border">
                                <legend className="scheduler-border"><div>ErrorCauseStatus</div></legend>
                                <div className="control-group">
                                    <div className="controls bootstrap-timepicker">
                                        {
                                            ((this.state.ErrorCauseStatus[0] !== undefined 
                                                && this.state.ErrorCauseStatus[0].connection) ? 
                                                    this.ErrorCauseStatus()
                                                        : "disconnect from printer.")
                                        }
                                    </div>
                                </div>
                            </fieldset>
                        </div>

                        <div className="col-sm-6">
                            <fieldset className="scheduler-border">
                                <legend className="scheduler-border"><div>OfflineCauseStatus</div></legend>
                                <div className="control-group">
                                    <div className="controls bootstrap-timepicker">
                                        {
                                            ((this.state.OfflineCauseStatus[0] !== undefined 
                                                && this.state.OfflineCauseStatus[0].connection) ? 
                                                    this.OfflineCauseStatus()
                                                        : "disconnect from printer.")
                                        }
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>

                <div hidden={this.state.PrinterOffline} className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12">
                            <fieldset className="scheduler-border">
                                <legend className="scheduler-border"><div>Print-Test</div></legend>
                                <div className="control-group">
                                    <div className="controls bootstrap-timepicker">
                                        <div className="row">
                                            <div className="col-sm-6">
                                                <Button style={{float: "right",fontSize: 30}}  onClick={this.handlePrint} variant="contained" color="primary" component="span">print</Button>
                                            </div>
                                            <div className="col-sm-6">
                                                <Button style={{float: "left",fontSize: 30}}  onClick={this.handleCut} variant="contained" color="primary" component="span">cut</Button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            
            </React.Fragment>
        )
    }
}

export default Printer;